#include <iostream>
#include <cstring>
#include <unistd.h>
#include <cmath>

#include "Polynom.h"

/* Доп. задание -- b */
/*
 * Исследовать работу кодера/декодера, когда выбрано некорректное
число информационных символов (𝑙 > 𝑘).
 * Для заданной
преподавателем 𝑙 указать все возможные вектора ошибок, для которых
𝑤(𝑒) ≤ 𝑑 − 1 и ошибки при этом не обнаруживаются
*/
long l = 0;
const std::string COLOR_RED = "\033[1;31m";
const std::string COLOR_DEFAULT = "\033[0m";

Polynom coder(const std::string &s_m, const std::string &s_g)
{
    std::cout << "------CODER------" << std::endl;
    Polynom m(s_m, 2);
    std::cout << "m(x): " << m;

    Polynom g(s_g, 2);
    std::cout << "g(x): " << g;

    std::cout << "r: " << g.getDegree() << std::endl;
    std::cout << "k: " << m.getDegree() + 1 << std::endl;

    if (l > 0)
    {
        std::cout << "l: " << l << std::endl;
    }

    m.shift(g.getDegree());
    std::cout << "Shifted m(x): " << m;

    Polynom res1 = m / g;
    Polynom c = res1.getRemainder();
    Polynom a = m + c;

    if (l > 0)
    {
        a.addZero();
    }

    std::cout << "\nCoder result a(x): " << a;
    std::cout << "Vector a: " << a.asVector() << std::endl;

    return a;
}

Polynom channel(const std::string &s_e, Polynom a, bool print)
{
    if (print) std::cout << "-----CHANNEL-----" << std::endl;
    Polynom e(s_e, 2);
    if (print) std::cout << "Vector e: " << e.asVector() << std::endl;
    if (print) std::cout << "Vector a: " << a.asVector() << std::endl;
    Polynom b = a + e;

    return b;
}


uint8_t decoder(const std::string &s_g, Polynom &b, bool print)
{
    if (print) std::cout << "-----DECODER-----" << std::endl;
    Polynom g(s_g, 2);
    if (print) std::cout << "g(x): " << g;
    if (print) std::cout << "b(x): " << b;
    if (print) std::cout << "Vector b: " << b.asVector() << std::endl;

    Polynom res2 = b / g;
    Polynom s = res2.getRemainder();

    if (print) std::cout << "S(x) is: " << s;

    uint8_t E = 0;

    if (!s.isZero())
    {
        if (print) std::cout << "There was error in channel. E = 1\n";
        E = 1;
    } else
    {
        if (print) std::cout << "There wasn't error in channel. E = 0\n";
        E = 0;
    }
    return E;
}

int main(int argc, char **argv)
{
    int temp_stdout = dup(1);
    int idx = 1;
    if (std::strcmp(argv[idx], "-s") == 0)
    {
        //freopen("output", "w", stdout);
        idx++;
    } else if (std::strcmp(argv[idx], "-t") == 0)
    {
        // Test Mode for Task B
        Polynom g("1011", 2);
        Polynom m("1000", 2);
        auto l = std::atoi(argv[++idx]);
        long N = std::pow(2, l + g.getDegree());

        std::cout << "------TEST------" << std::endl;
        std::cout << "m(x): " << m;
        std::cout << "g(x): " << g;
        std::cout << "r: " << g.getDegree() << std::endl;
        std::cout << "k: " << m.getDegree() + 1 << std::endl;
        std::cout << "l: " << l << std::endl;
        std::cout << "Number of unique e_str vectors: " << N << std::endl;

        auto k = m.getDegree() + 1;
        m.shift(g.getDegree());
        std::cout << "Shifted m(x): " << m;

        Polynom res1 = m / g;
        Polynom c = res1.getRemainder();
        Polynom a = m + c;
        std::string e_str;

        for (int i = k; i < l; i++)
        {
            a.addZero();
        }

        for (int i = 0; i < l + g.getDegree(); i++)
        {
            e_str.push_back('0');
        }

        std::cout << "\nCoder result a(x): " << a;
        std::cout << "Vector a: " << a.asVector() << std::endl;

        a.setLoud(false);

        // Generate e_str
        bool rided = false;
        auto j = e_str.length();

        auto counter = 0;
        while (counter < N - 1)
        {
            e_str[j] = '1';
            while (j >= 0 && e_str[j] == '1')
            {
                e_str[j] = '0';
                j -= 1;
                rided = true;
            }

            if (rided && j >= 0)
            {
                e_str[j] = '1';
                j = e_str.length();
                rided = false;
                counter++;
                //count weight

                auto w = 0;
                for (int i = 0; i <= e_str.length(); i++)
                {
                    w += (e_str[i] == '1') ? 1 : 0;
                }
                if (w <= g.getDegree() - 1)
                {
                    Polynom b = channel(e_str, a, false);
                    b.setLoud(false);
                    uint8_t E = decoder("1011", b, false);
                    if (E == 0)
                    {
                        Polynom b_new = channel(e_str, a, true);
                        b_new.setLoud(true);
                        E = decoder("1011", b_new, true);
                        std::cout << COLOR_RED << "DECODER ERROR: " << COLOR_DEFAULT << e_str << std::endl;
                    }
                }

            }
        }

        std::cout << counter;


        return 0;
    }

    std::string g = argv[idx++];
    std::string e = argv[idx++];
    std::string m = argv[idx++];
    l = std::atoi(argv[idx]);

    Polynom a = coder(m, g);
    Polynom b = channel(e, a, true);
    uint8_t E = decoder(g, b, true);

    return 0;
}