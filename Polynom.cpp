#include <algorithm>
#include "Polynom.h"


Polynom::Polynom()
        : data(), field(0)
{
}

Polynom::Polynom(int size, int f)
        : data(), field(f)
{
    while (size)
    {
        size--;
        data.push_back(0);
    }
}

Polynom::Polynom(std::string polynom, int f)
        : data(), field(f)
{
    std::reverse(polynom.begin(), polynom.end());
    std::istringstream iss(polynom);
    std::string token;

    char chr;
    while (iss >> chr)
    {
        if (field)
            data.push_back(Mod::mod(std::atoi(&chr), field));
        else
            data.push_back(std::atoi(&chr));
    }

    /*while (std::getline(iss, token, '_'))
    {
        if (field)
            data.push_back(Mod::mod(std::atoi(token.c_str()), field));
        else
            data.push_back(std::atoi(token.c_str()));
    }*/
}

Polynom::Polynom(std::vector<int> &v, int f)
{
    field = f;
    data = v;
}

Polynom::Polynom(const Polynom &p)
{
    data.clear();
    remainder = p.remainder;
    field = p.field;
    data = p.data;
}

Polynom::~Polynom()
{
}

Polynom &Polynom::operator=(const Polynom &p)
{
    remainder = p.remainder;
    field = p.field;
    data = p.data;
    return *this;
}

Polynom Polynom::operator+(const Polynom &p)
{
    std::vector<int>::const_iterator p_iter;
    for (iter = data.begin(), p_iter = p.data.begin(); iter != data.end() && p_iter != p.data.end(); iter++, p_iter++)
    {
        if (field)
            (*iter) = Mod::mod(((*iter) + (*p_iter)), field);
        else
            (*iter) = ((*iter) + (*p_iter));
    }

    return *this;
}

Polynom Polynom::operator-(Polynom &p)
{
    for (iter = data.begin(), p.iter = p.data.begin(); iter != data.end() && p.iter != p.data.end(); iter++, p.iter++)
    {
        if (field)
            (*iter) = Mod::mod(((*iter) - (*p.iter)), field);
        else
            (*iter) = ((*iter) - (*p.iter));
    }

    return *this;
}


Polynom Polynom::operator*(Polynom &p)
{
    Polynom result(data.size() + p.data.size(), p.field);

    for (int i = 0; i < data.size(); i++)
    {
        for (int j = 0; j < p.data.size(); j++)
        {
            if (field)
            {
                result.data[i + j] = Mod::mod(result.data[i + j] + (data[i] * p.data[j]), field);
            }
            else
            {
                result.data[i + j] = result.data[i + j] + (data[i] * p.data[j]);
            }
        }
    }


    return result;
}

Polynom Polynom::operator/(Polynom &p)
{
    Polynom substract(data.size(), 2);
    Polynom result(data.size() + p.data.size(), 2);
    Polynom dividend(*this);
    Polynom divider(p);
    int i = 0;
    while (dividend.data.size() - divider.data.size() - i + 1 > 0)
    {
        Polynom tmp_res(data.size() + p.data.size(), 2);
        if (field)
        {
            tmp_res.data[dividend.data.size() - divider.data.size() - i] = static_cast<int>(Mod::inverse(
                                (dividend.data[dividend.data.size() - i - 1]), divider.data.back(), field));
        } else
        {
            tmp_res.data[dividend.data.size() - divider.data.size() - i] =
                    (dividend.data[dividend.data.size() - i - 1]) / divider.data.back();
        }

        result = result + tmp_res;
        substract = tmp_res * divider;
        if (!substract.isZero())
        {
            if (loud) std::cout << dividend;
            if (loud) std::cout << substract;
            if (loud) std::cout << "----------------" << std::endl;

        }
        dividend = dividend - substract;
        i++;
    }
    if (loud) std::cout << dividend;
    result.remainder = dividend.data;
    result.setField(p.getField());

    return result;
}

Polynom &Polynom::undefinedCoef(Polynom &divider)
{
    *this = *this / divider;
    return *this;
}

std::ostream &operator<<(std::ostream &o, Polynom &p)
{
    std::ostringstream stream;
    for (p.iter = p.data.begin(); p.iter != p.data.end(); p.iter++)
    {
        if ((*p.iter) != 0 && (*p.iter) != p.field)
        {
            stream << " ";
            (*p.iter) != 1 ? stream << (*p.iter) : stream;
            auto num = p.iter - p.data.begin();
            std::string num_str = std::to_string(num);
            if (num > 9)
            {
                std::reverse(num_str.begin(), num_str.end());
            }
            stream << num_str << "^x" << " +";
        }
    }

    std::string str = stream.str();
    if (str.back() == '+')
    {
        str.resize(stream.str().size() - 1);
        std::reverse(str.begin(), str.end());
    }
    o << str << std::endl;

    if (p.remainder.size())
    {
        o << "REMAINDER: ";
        std::ostringstream remstream;
        for (p.iter = p.remainder.begin(); p.iter != p.remainder.end(); p.iter++)
        {
            if ((*p.iter) != 0)
            {
                if ((*p.iter) != 0)
                {
                    remstream << " ";
                    (*p.iter) != 1 ? stream << (*p.iter) : remstream;
                    remstream << "x^" << p.iter - p.remainder.begin() << " +";
                }
            }
        }
        std::string remstr = remstream.str();
        remstr.resize(remstream.str().size() - 1);

        o << remstr << std::endl;
    }

    return o;
}

void Polynom::shift(int num)
{
    for (int i = 0; i < num; i++)
    {
        data.emplace(data.begin(), 0);
    }
}

bool Polynom::isZero()
{
    for (int i = 0; i < data.size(); i++)
    {
        if (data[i] != 0)
            return false;
    }
    return true;
}

std::string Polynom::asVector()
{
    std::ostringstream stream;
    for (auto i = data.rbegin(); i != data.rend(); i++)
    {
        stream << (*i) << " ";
    }
    stream << std::endl;
    return stream.str();
}
